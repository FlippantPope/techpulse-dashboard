<?php
/**
*|--------------------------------------------------------------------------
*| Web Routes
*|--------------------------------------------------------------------------
*|
*| Here is where you can register web routes for your application. These
*| routes are loaded by the RouteServiceProvider within a group which
*| contains the "web" middleware group. Now create something great!
*|.
*/

// header('Access-Control-Allow-Origin:  *');

Auth::routes();

Route::name('dashboard.')->group(function () {
    Route::group(['middleware' => ['auth']], function () {
        // Admin Dashboard Index page
        Route::get('/', 'DashboardController@index')->name('index');

        // TODO: Change to Users Controller
        // Admin Dashboard Users Table
        Route::get('/users', 'DashboardController@users')->name('users');


        // TODO: Change to Sites Controller
        // Admin Dashboard Sites Table
        Route::get('/sites', 'DashboardController@sites')->name('sites.index');

        // Show Site Edit form
        Route::get('/sites/{site}/edit', 'Admin\SiteController@edit')->name('sites.edit');

        // Show create Site form
        Route::get('/site/new', 'Admin\SiteController@create')->name('sites.create');

        // POST Site edits
        Route::put('/sites/{site}', 'Admin\SiteController@update')->name('sites.update');

        // POST Site create
        Route::post('/sites', 'Admin\SiteController@store')->name('sites.store');

        // POST delete SITE
        Route::delete('/sites/{site}', 'Admin\SiteController@destroy')->name('sites.destroy');

        Route::name('profile.')->group(function () {
            Route::get('/profile', 'ProfileController@index')->name('index');
        });

        Route::get('/oauth', function () {
            return view('admin.oauth.index');
        })->name('oauth');
    });
});
