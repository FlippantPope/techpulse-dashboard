# Techpulse Dashboard
> Application to view and manage a landing page.

[![Build Status][travis-image]][travis-url]


## Development setup

Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.


## Release History

* 0.0.1
    * Work in progress

## Meta

Jonathan Middleton – jonpmiddleton@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://github.com/FlippantPope/techpulse-dashboard](https://gitlab.com/FlippantPope/)

## Contributing

1. Fork it (<https://gitlab.com/FlippantPope/techpulse-dashboard/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[travis-image]: https://travis-ci.org/FlippantPope/techpulse-dashboard.svg?branch=master
[travis-url]: https://travis-ci.org/FlippantPope/techpulse-dashboard
[wiki]: https://gitlab.com/FlippantPope/yourproject/wiki
