// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parserOptions: {
      parser: 'babel-eslint',
      sourceType: 'module'
    },
    env: {
      browser: true,
      es6: true,
      commonjs: true
    },
    // required to lint *.vue files
    plugins: ['vue'],
    extends: [
      'eslint:recommended',
      'plugin:vue/recommended',
      'prettier',
      'plugin:import/errors',
      'plugin:import/warnings'
    ],
    // add your custom rules here
    'rules': {
      'no-param-reassign': 0,
      'no-multi-assign': 0,
      'indent': ['error', 2],
      'quotes': ['error', 'single'],
      'semi': ['error', 'always', { 'omitLastInOneLineBlock': true }],
      'no-empty': 0,
      'no-undef': 0,
      'no-unused-vars': 0,
      'vue/require-default-prop': 0,

      'vue/max-attributes-per-line': [2, {
          'singleline': 2,
          'multiline': {
              'max': 1,
              'allowFirstLine': true
          }
      }],

      // don't require .vue extension when importing
      'import/extensions': ['error', 'always', {
        'js': 'never',
        'vue': 'never'
      }],
      // allow debugger during development
      'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
      'no-alert': process.env.NODE_ENV === 'production' ? 2 : 0,
      'no-console': process.env.NODE_ENV === 'production' ? 2 : 0
    }
  }
