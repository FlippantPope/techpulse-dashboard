<?php

use Faker\Generator as Faker;

$factory->define(App\Site::class, function (Faker $faker) {
    return [
        'title' => $faker->domainName,
        'url' => $faker->url,
        'screenshot' => 'img/thumb.png',
        'click_amt' => $faker->numberBetween(0, 50),
    ];
});
