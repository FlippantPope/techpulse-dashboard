<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Personal'
        ]);
        DB::table('categories')->insert([
            'name' => 'Comics'
        ]);
        DB::table('categories')->insert([
            'name' => 'Entertainment'
        ]);
        DB::table('categories')->insert([
            'name' => 'Shopping'
        ]);
        DB::table('categories')->insert([
            'name' => 'Bills'
        ]);
        DB::table('categories')->insert([
            'name' => 'Banking'
        ]);
        DB::table('categories')->insert([
            'name' => 'EVE'
        ]);
        DB::table('categories')->insert([
            'name' => 'Elite Dangerous'
        ]);
    }
}
