<?php

use Illuminate\Database\Seeder;

class SiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        factory(App\Site::class, 10)->create()->each(function ($site) {
            $site->category()->associate(rand(1, 8))->save();
        });
    }
}
