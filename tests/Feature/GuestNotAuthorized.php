<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GuestNotAuthorized extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGuestNotAuthorized()
    {
        $response = $this->visit('/');

        $response->assertStatus(302);
    }
}
