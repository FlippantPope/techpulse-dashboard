@servers(['web' => 'staging-server'])

@setup

    $repo = "git@gitlab.com:FlippantPope/techpulse-dashboard.git";
    $releases_dir = "/var/www/techpulse.us/releases";
    $app_dir = "/var/www/techpulse.us";
    $release = date('YmdHis');
    $new_release_dir = $releases_dir . "/" . $release;

@endsetup

@story('deploy-staging')
    clone_repo
    install_dep
    update_symlinks
    run_artisan
    compile_assets
@endstory

@task('clone_repo')
    echo "Clearing previous releases"
    rm -R {{ $releases_dir }}/*
    echo "Cloning Repo..."
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repo }} {{ $new_release_dir }}
@endtask

@task('install_dep')
    echo "Installing dependencies for {{ $release }}..."
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking {{ $release }} storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo "Creating Site storage directory..."
    rm -rf {{ $new_release_dir }}/public/img/sites
    ln -nfs {{ $app_dir }}/storage/app/sites {{ $new_release_dir }}/public/img/sites

    echo 'Linking {{ $relase }} .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking {{ $new_release_dir }} to staging'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/staging
@endtask

@task('run_artisan')
    cd {{ $new_release_dir }}
    php artisan key:generate
    php artisan cache:clear
    php artisan config:cache
    php artisan migrate
@endtask

@task('compile_assets')
    cd {{ $new_release_dir }}
    npm install
    npm run prod
@endtask
