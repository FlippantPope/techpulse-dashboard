<?php
/**
 * Site Model.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Site Model class.
 */
class Site extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'picture', 'added_by',
    ];

    /**
     * The attributes are not assignable.
     *
     * @var array
     */
    protected $attributes = [
        'click_amt' => 0,
    ];

    /**
     * A site belongs to one \App\Category.
     *
     * @return Model
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'favorites');
    }
}
