<?php
namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoriteController extends Controller
{
    public function index(Request $request, User $user)
    {
        $authUser = auth()->user();

        if ($authUser->favorites()->count() > 0) {
            return auth()->user()->favorites()->get();
        } else {
            return 0;
        }
    }
}
