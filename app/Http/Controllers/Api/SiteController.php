<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Site;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Site::with('category')->get()->jsonSerialize();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => 'required',
            'category' => 'required',
            'url' => 'required|url',
            'screenshot' => 'image|required',
        ]);

        $site = new Site();

        $site->title = $request->title;

        $site->category = $request->category;

        $site->url = $request->url;

        $screenshotPath = $request->screenshot->store('sites');

        $site->screenshot = $screenshotPath;

        $site->save();

        return $site->jsonSerialize();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        return $site;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Site                $site
     *
     * @return \Illuminate\Http\Response
     *
    public function update(Request $request, Site $site)
    {
        $validate = $request->validate([
            'title' => 'string',
            'category' => 'digits:1',
            'url' => 'url',
            'screenshot' => 'image',
        ]);

        $site->title = $request->title;

        $site->category = $request->category;

        $site->url = $request->url;

        if ($request->hasFile('screenshot')) {
            $path = $request->screenshot->store('sites');
            $site->screenshot = $path;
        }
        $site->save();

        return 'Success!!';
    }
     */
    public function incrementClick(Request $request, Site $site)
    {
        $site->increment('click_amt');
        $site->save();

        return $site;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
    }
}
