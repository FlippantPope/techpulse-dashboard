<?php

namespace App\Http\Controllers\Admin;

use App\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sites.index', [
            'sites' => Site::all(),
            'user' => Auth::user(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sites.create', ['user' => Auth::user()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => 'required',
            'category' => 'required',
            'url' => 'required|url',
            'screenshot' => 'image|required',
        ]);
        $site = new Site();

        $site->title = $request->title;

        $site->category()->associate($request->category);

        $site->url = $request->url;

        $screenshotPath = $request->screenshot->store('sites');

        $site->screenshot = $screenshotPath;

        $site->save();

        return back()->with('status', 'Site Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        return $site;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.sites.edit', [
            'site' => Site::findOrFail($id),
            'user' => Auth::user(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Site                $site
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        $validate = $request->validate([
            'title' => 'required',
            'category' => 'digits:1',
            'url' => 'url',
            'screenshot' => 'mimes:jpeg,bmp,png',
        ]);

        $site->title = $request->title;

        $site->category()->associate($request->category);

        $site->url = $request->url;

        if ($request->hasFile('screenshot')) {
            $path = $request->screenshot->store('sites');
            $site->screenshot = $path;
        }

        if ($site->isClean()) {
            // Site UNCHANGED
            return back();
        } else {
            // Site CHANGED
            $site->save();

            return back()->with('status', 'Site Updated Successfully');
        }
    }

    public function incrementClick(Request $request, Site $site)
    {
        $site->increment('click_amt');
        $site->save();

        return $site;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        $site->delete();

        return back()->with('status', 'Site Deleted');
    }
}
