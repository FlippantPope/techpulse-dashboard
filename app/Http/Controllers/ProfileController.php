<?php
/**
 * Profile Controller
 *
 */
namespace App\Http\Controllers;
use App\Site;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Controller for Profile Pages
 */
class ProfileController extends Controller
{
    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Index function
     *
     * @return Illuminate\Http\Request
     */
    public function index()
    {
        return view('admin.profile.index', [ 'user' => Auth::user() ]);
    }
}
