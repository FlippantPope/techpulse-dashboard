<?php

namespace App\Http\Controllers;

use App\Site;
use App\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topTenLinks = Site::orderBy('click_amt', 'desc')
                    ->take(10)
                    ->get();

        return view('admin.index', [
            'links' => $topTenLinks,
            'user' => Auth::user(),
        ]);
    }

    /**
     * Return sites page.
     *
     * @return \Illuminate\Http\Resource
     */
    public function sites()
    {
        return view('admin.sites.index', [
            'sites' => Site::all(),
            'user' => Auth::user(),
        ]);
    }

    /**
     * Return Users page.
     *
     * @return \Illuminate\Http\Resource
     */
    public function users()
    {
        return view('admin.users.index', [
            'users' => User::all(),
            'user' => Auth::user(),
        ]);
    }
}
