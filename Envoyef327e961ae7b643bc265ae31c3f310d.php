<?php $relase = isset($relase) ? $relase : null; ?>
<?php $new_release_dir = isset($new_release_dir) ? $new_release_dir : null; ?>
<?php $release = isset($release) ? $release : null; ?>
<?php $app_dir = isset($app_dir) ? $app_dir : null; ?>
<?php $releases_dir = isset($releases_dir) ? $releases_dir : null; ?>
<?php $repo = isset($repo) ? $repo : null; ?>
<?php $__container->servers(['web' => 'staging-server']); ?>

<?php

    $repo = "git@gitlab.com:FlippantPope/techpulse-dashboard.git";
    $releases_dir = "/var/www/techpulse.us/releases";
    $app_dir = "/var/www/techpulse.us";
    $release = date('YmdHis');
    $new_release_dir = $releases_dir . "/" . $release;

?>

<?php $__container->startMacro('deploy-staging'); ?>
    clone_repo
    install_dep
    update_symlinks
<?php $__container->endMacro(); ?>

<?php $__container->startTask('clone_repo'); ?>
    echo "Cloning Repo..."
    [ -d <?php echo $releases_dir; ?> ] || mkdir <?php echo $releases_dir; ?>

    git clone --depth 1 <?php echo $repo; ?> <?php echo $new_release_dir; ?>

<?php $__container->endTask(); ?>

<?php $__container->startTask('install_dep'); ?>
    echo "Installing dependencies for <?php echo release; ?>..."
    cd <?php echo $new_release_dir; ?>

    composer install --prefer-dist --no-script -q -o
<?php $__container->endTask(); ?>

<?php $__container->startTask('update_symlinks'); ?>
    echo "Linking <?php echo $release; ?> storage directory"
    rm -rf <?php echo $new_release_dir; ?>/storage
    ln -nfs <?php echo $app_dir; ?>/storage <?php echo $new_release_dir; ?>/storage

    echo "Creating Site storage directory..."
    mkdir <?php echo $app_dir; ?>/storage/sites
    rm -rf <?php echo $new_release_dir; ?>/public/img/sites
    ln -nfs <?php echo $new_release_dir; ?>/current/public/img/sites <?php echo $app_dir; ?>/storage/sites

    echo 'Linking <?php echo $relase; ?> .env file'
    ln -nfs <?php echo $app_dir; ?>/.env <?php echo $new_release_dir; ?>/.env

    echo 'Linking <?php echo $new_release_dir; ?> to staging'
    ln -nfs <?php echo $new_release_dir; ?> <?php echo $app_dir; ?>/staging
<?php $__container->endTask(); ?>
