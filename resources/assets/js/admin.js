/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');




window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 *
 */

var statusElement = document.getElementById('statusMsg');

// Event Listener for Cancel on Create Site form
if (document.getElementById('siteCreateCancelBtn')) {
  document.getElementById('siteCreateCancelBtn').addEventListener('click',
    function(event) {
      // Reset placeholder for bootstrap custom element
      document.getElementById('fileInputLabel').innerHTML =
        'Pick a Screenshot';
      // Reest Form elements
      event.toElement.form.reset();
    }, false);
}

// Event for reseting success or error messages

window.addEventListener('load', function(event) {
  if (document.getElementById('statusMsg')) {
    setTimeout(function() {
      fadeOut(statusElement);
    }, 5000);
  }
}, false);


window.$('input[type=file]').change(function() {

  var fieldVal = window.$(this).val();

  // Change the node's value by removing the fake path (Chrome)
  fieldVal = fieldVal.replace('C:\\fakepath\\', '');

  if (fieldVal != undefined || fieldVal != '') {
    window.$(this).next('.custom-file-input').attr('data-content', fieldVal);
    window.$(this).next('.custom-file-label').text(fieldVal);
  }

});

function fadeOut(element) {
  element.style.opacity = 1;
  (function fade() {
    if ((element.style.opacity -= .1) < 0) {
      element.style.display = 'none';
    } else {
      setTimeout(fade, 100);
    }
  })();
}

// Passport Components
Vue.component('passport-clients', require('./components/passport/Clients.vue'));
Vue.component('passport-authorized-clients', require(
  './components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens', require(
  './components/passport/PersonalAccessTokens.vue'));

const app = new Vue({
  el: '#app',
});

