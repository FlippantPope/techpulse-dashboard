/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/*
window.$('input[type=file]').change(function() {

  var fieldVal = window.$(this).val();

  // Change the node's value by removing the fake path (Chrome)
  fieldVal = fieldVal.replace('C:\\fakepath\\', '');

  if (fieldVal != undefined || fieldVal != '') {
    window.$(this).next('.custom-file-input').attr('data-content', fieldVal);
    window.$(this).next('.custom-file-label').text(fieldVal);
  }

});
*/
window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('site-cards', require('./components/SiteCards.vue'));
Vue.component('sites-list', require('./components/SitesList.vue'));
Vue.component('sort-dropdown', require('./components/SortDropdown.vue'));

const app = new Vue({
  el: '#app',
  data: {
    sites: [],
    sortedByClicks: false,
    sortedByName: false
  },
  created() {
    this.fetchSites();
  },
  methods: {
    fetchSites() {
      axios.get('api/sites').then(res => {
        this.sites = res.data;
        this.sortByClicks();
      });
    },
    sortByName() {
      this.sortedByName = true;
      this.sortedByClicks = false;
      let sortedSites = _.orderBy(this.sites, 'title', 'asc');
      this.sites = sortedSites;
    },
    sortByClicks() {
      this.sortedByClicks = true;
      this.sortedByName = false;
      let sortedSites = _.orderBy(this.sites, 'click_amt', 'desc');
      this.sites = sortedSites;
    }
  },
});
