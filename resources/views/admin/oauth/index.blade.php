@extends('layouts.admin')
@section('content') @if (session('status'))
<div id="statusMsg" class="alert alert-success status-msg">{{ session('status') }}</div>
@endif

<h1>Personal Access Tokens</h1>

    <passport-personal-access-tokens></passport-personal-access-tokens>

@endsection
