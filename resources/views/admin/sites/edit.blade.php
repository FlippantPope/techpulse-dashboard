@extends('layouts.admin')
@section('content')

@if (session('status'))
<div id="statusMsg" class="alert alert-success status-msg">{{ session('status') }}</div>
@endif

<h1>Edit Site</h1>
<form id="editForm" method="POST" enctype="multipart/form-data" action="{{ route('dashboard.sites.update', [ 'site' => $site->id ]) }}">
    @method('PUT') @csrf
    <div class="form-group row">
        <label for="title" class="col-sm-1 col-form-label">Title</label>
        <div class="col-sm-10">
            <input id="title" name="title" type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" value="{{ old('title', $site->title) }}">
            @foreach ($errors->get('title') as $message)
                <div class="invalid-feedback">{{ $message }} </div>
            @endforeach
        </div>

    </div>
    <div class="form-group row">
        <label for="category" class="col-sm-1 col-form-label">Category</label>
        <div class="col-sm-10">
            <select id="category" name="category" class="form-control {{ $errors->has('category') ? 'is-invalid' : '' }}">
                @if ($site->category === 1)
                    <option value="1" selected>Personal</option>
                @else
                    <option value="1">Personal</option>
                @endif
                @if ($site->category === 2)
                    <option value="2" selected>Comics</option>
                @else
                    <option value="2">Comics</option>
                @endif
                @if ($site->category === 3)
                    <option value="3" selected>Entertainment</option>
                @else
                    <option value="3">Entertainment</option>
                @endif
                @if ($site->category === 4)
                    <option value="4" selected>Shopping</option>
                @else
                    <option value="4">Shopping</option>
                @endif
                @if ($site->category === 5)
                    <option value="5" selected>Bills</option>
                @else
                    <option value="5">Bills</option>
                @endif
                @if ($site->category === 6)
                    <option value="6" selected>Banking</option>
                @else
                <option value="6">Banking</option>
                @endif
                @if ($site->category === 7)
                    <option value="7" selected>EVE</option>
                @else
                    <option value="7">EVE</option>
                @endif
                @if ($site->category === 8)
                    <option value="8" selected>Elite Dangerous</option>
                @else
                    <option value="8">Elite Dangerous</option>
                @endif
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="url" class="col-sm-1 col-form-label">URL</label>
        <div class="col-sm-10">
            <input id="url" name="url" type="text" class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}" value="{{ old('url', $site->url) }}">
            @foreach ($errors->get('url') as $message)
                <div class="invalid-feedback">{{ $message }} </div>
            @endforeach
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-1 col-form-label">Screenshot</label>
        <div class="col-sm-10">
            <div class="custom-file">
                <input id="screenshot" class="custom-file-input {{ $errors->has('screenshot') ? 'is-invalid' : '' }}" name="screenshot" type="file"
                    value="{{ old('screenshot', $site->screenshot) }}" accept="image/*">
                <label for="screenshot" class="custom-file-label">{{ old('screenshot', $site->screenshot) }}</label>
                @foreach ($errors->get('screenshot') as $message)
                <div class="invalid-feedback">{{ $message }} </div>
                @endforeach
            </div>
        </div>
    </div>
    <button class="btn btn-primary" type="button">Cancel</button>
    <button class="btn btn-primary" type="submit">Submit</button>
</form>
@endsection
