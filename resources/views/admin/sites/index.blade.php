@extends('layouts.admin') 
@section('content')

@if (session('status'))
<div id="statusMsg" class="alert alert-success status-msg">{{ session('status') }}</div>
@endif

<div class="row">
    <div class="col">
        <h1>Sites</h1>
    </div>
</div>
<div class="row">
    <div class="table-responsive col-11 col-sm-12">
        <table class="table table-striped table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Category</th>
                    <th scope="col">Image</th>
                    <th scope="col">Visits</th>
                    <th scope="col">Actions</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sites as $site)
                <tr>
                    <td scope="row" class="align-middle">{{ $site->title }}</td>
                    <td class="align-middle">{{ $site->category->name }}</td>
                    <td class="align-middle">{{ $site->screenshot }}</td>
                    <td class="align-middle">{{ $site->click_amt }}</td>
                    <td class="align-middle">
                        <a href="{{ route('dashboard.sites.edit', ['site' => $site->id] ) }}" class="btn btn-primary btn-block">Edit</a>
                    </td>
                    <td class="align-middle">
                        <form method="POST" action="{{ route('dashboard.sites.destroy', [ 'site'=> $site->id ]) }}">
                            @csrf
                            @method('delete')
                            
                            <button class="btn btn-danger btn-block" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection