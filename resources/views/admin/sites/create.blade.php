@extends('layouts.admin')
@section('content')

@if (session('status'))
<div id="statusMsg" class="alert alert-success status-msg">{{ session('status') }}</div>
@endif

<div class="row">
    <div class="col">
        <h1>Create Site</h1>
    </div>
</div>
<div class="row">
    <div class="col">
        <form id="siteCreateFrm" method="POST" action="{{ route('dashboard.sites.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label for="inputTitle" class="col-sm-1 col-form-label">Title</label>
                <div class="col-sm-10">
                    <input id="inputTitle" name="title" type="text" placeholder="Enter Title" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputCategory" class="col-sm-1 col-form-label">Category</label>
                <div class="col-sm-10">
                    <select id="inputCategory" name="category" class="form-control">
                    <option value="1">Personal</option>
                    <option value="2">Comics</option>
                    <option value="3">Entertainment</option>
                    <option value="4">Shopping</option>
                    <option value="5">Bills</option>
                    <option value="6">Banking</option>
                    <option value="7">EVE</option>
                    <option value="8">Elite Dangerous</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputUrl" class="col-sm-1 col-form-label">URL</label>
                <div class="col-sm-10">
                    <input id="inputUrl" type="text" name="url" placeholder="http://www.example.com" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-1 col-form-label">Screenshot</label>
                <div class="col-sm-10">
                    <div class="custom-file">
                        <input id="fileInput" type="file" name="screenshot" class="custom-file-input">
                        <label for="screenshotFile" class="custom-file-label" id="fileInputLabel">Pick Screenshot</label>
                    </div>
                </div>
            </div>
            <button class="btn btn-danger" type="button" id="siteCreateCancelBtn">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>
</div>
@endsection
