@extends('layouts.admin')
@section('content')
<h1>Dashboard</h1>
<h2 class="sub-header">Top Ten</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Link</th>
                <th>Image</th>
                <th>Category</th>
                <th>Visits</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($links as $site)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $site->title }}</td>
                <td>{{ $site->url }}</td>
                <td>{{ $site->screenshot }}</td>
                <td>{{ $site->category->name }}</td>
                <td>{{ $site->click_amt }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
