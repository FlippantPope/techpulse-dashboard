<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta id="csrf-token" name="csrf-token" value="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <header>
            @include('layouts.nav')
        </header>

        <div class="container-fluid">
            <div class="row">
                @auth
                    @if (!Route::currentRouteNamed('dashboard.profile.*'))
                        @include('layouts.sidenav')
                    @endif
                        <main role="main" class="col-sm-12 ml-sm-auto col-md-10 pt-3">
                @endauth
                @guest
                    <main role="main" class="col-md-12 pt-3">
                @endguest
                @yield('content')
                </main>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
</body>

</html>
