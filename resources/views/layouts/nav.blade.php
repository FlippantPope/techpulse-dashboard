<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ route('dashboard.index') }}">{{ config('app.name') }}</a>

    <div id="navbar" class="navbar-collapse collapse">
        @auth
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ (!Route::currentRouteNamed('dashboard.profile.*')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.index') }}">Dashboard</a>
            </li>
            <li class="nav-item {{ (Route::currentRouteNamed('dashboard.profile.*')) ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard.profile.index') }}">Profile</a>
            </li>
        </ul>
        @endauth @guest
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
        </ul>
        @else
        <div class="dropdown">
            <button id="userDropdownBtn" class="btn btn-dark dropdown-toggle" type="button" data-toggle="dropdown">{{ Auth::user()->name }}</button>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('dashboard.sites.create') }}" class="dropdown-item">Add New Site</a>
                <a class="dropdown-item text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
            </div>
        </div>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @endguest
    </div>
</nav>
