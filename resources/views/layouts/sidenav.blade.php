<nav class="col-sm-3 col-md-2 d-none d-md-block sidebar border-primary">
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <a class="nav-link {{ (Route::currentRouteNamed('dashboard.index')) ? 'active' : '' }}" href="{{ route('dashboard.index') }}">
                Overview
            </a>
        </li>
    </ul>
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        Sites
    </h6>
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <a class="nav-link {{ (Route::currentRouteNamed('dashboard.sites.index')) ? 'active' : '' }}" href="{{ route('dashboard.sites.index') }}">
                View Sites
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ (Route::currentRouteNamed('dashboard.sites.create')) ? 'active' : '' }}" href="{{ route('dashboard.sites.create') }}">
                Add New Site
            </a>
        </li>
    </ul>
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        Users
    </h6>
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <a class="nav-link {{ (Route::currentRouteNamed('dashboard.users')) ? 'active' : '' }}" href="{{ route('dashboard.users') }}">
                View Users
            </a>
        </li>
    </ul>
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
            <a href="{{ route('dashboard.oauth') }}" class="nav-link {{ (Route::currentRouteNamed('dashboard.oauth')) ? 'active' : '' }}">OAuth</a>
        </li>
    </ul>
</nav>
